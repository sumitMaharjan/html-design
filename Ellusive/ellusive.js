 // navbar
const nav=document.querySelectorAll('nav')[0];
const mybars=document.querySelectorAll('.mybars')[0];
const menu=document.querySelectorAll('.menu')[0];
const biglist=document.querySelectorAll('.biglist');
const scrollTop=document.querySelectorAll('.scrollTop')[0];

function showSubmenu(){
        this.lastElementChild.classList.add('d-block');
}
function hideSubmenu(){
        this.lastElementChild.classList.remove('d-block');
}
function forHoverEvent(){
    biglist.forEach((x,i)=>{
        biglist[i].addEventListener('mouseenter',showSubmenu);
        biglist[i].addEventListener('mouseleave',hideSubmenu);
    })
}
function forsubmenu(){
    // arrow   
   this.firstElementChild.lastElementChild.classList.toggle("arrow_rotate");

   // sublists
   this.lastElementChild.classList.toggle('d-block');
   this.lastElementChild.classList.toggle('submenupos');
}

function resizeJob(){
    if(window.innerWidth<800){
     biglist.forEach((x,i)=>{
            biglist[i].addEventListener('click',forsubmenu);
    })
    biglist.forEach((x,i)=>{
            biglist[i].removeEventListener('mouseenter',showSubmenu);
            biglist[i].removeEventListener('mouseleave',hideSubmenu);
    })
    }
    else{
        biglist.forEach((x,i)=>{
            biglist[i].removeEventListener('click',forsubmenu);

            biglist[i].addEventListener('mouseenter',showSubmenu);
            biglist[i].addEventListener('mouseleave',hideSubmenu);
    })
    }
}

forHoverEvent();

mybars.onclick=()=>{
    menu.classList.toggle('slideLeft');
}


window.onscroll=()=>{
    if(document.documentElement.scrollTop>100)
        scrollTop.classList.add('fadeUp');
    else
        scrollTop.classList.remove('fadeUp');
}
scrollTop.onclick=()=>{
    window.scrollTo(0,0);
}
window.onresize=()=>{
    if(menu.classList.contains('slideLeft')){
        menu.classList.remove('slideLeft');
    }
   resizeJob();
}


// banner
var banner=document.querySelectorAll('.banner')[0];
var bannerH2=document.querySelectorAll('.banner h2')[0];
var bannerPara=document.querySelectorAll('.banner p')[0];
var bannerBtn=document.querySelectorAll('.banner button')[0];
var bannerImg=document.querySelectorAll('.banner>img')[0];
var bannerBg=[
    {bg:'wineGlasses.jpg',
     "heading":"this is couple heading",
     "para":"couple is beautiful couple is beautifulcouple is beautifulcouple is beautiful couple is beautiful"
    },
    {bg:'wineCouple.jpeg',
     "heading":"this is elephant heading",
     "para":"elephant is beautiful couple is beautifulcouple is beautifulcouple is beautiful couple is beautiful"
    },
    {bg:'beer.jpeg',
     "heading":"this is puppy heading",
     "para":"puppy is beautiful couple is beautifulcouple is beautifulcouple is beautiful couple is beautiful"
    }
];
var ind=0;
var banSlider=setInterval(()=>{banloop()},5000);

function banloop(){
    bannerImg.src="./images/"+bannerBg[ind].bg;
    setTimeout(()=>{bannerH2.className="fadeIn"},100);
    setTimeout(()=>{bannerPara.className="fadeIn"},200);
    setTimeout(()=>{bannerBtn.className="fadeIn"},400);

    setTimeout(()=>{bannerH2.className=""},4500);
    setTimeout(()=>{bannerPara.className=""},4700);
    setTimeout(()=>{bannerBtn.className=""},4900);
    ind++;
    if(ind==bannerBg.length){ind=0;}
}

banner.addEventListener('mouseover',()=>{
    clearInterval(banSlider);
});
banner.addEventListener('mouseout',()=>{
    banSlider=setInterval(()=>{banloop()},5000);

})

